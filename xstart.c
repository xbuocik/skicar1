/*
	Remember to compile try:
		1) gcc hi.c -o hi -lX11
		2) gcc hi.c -I /usr/include/X11 -L /usr/X11/lib -lX11
		3) gcc hi.c -I /where/ever -L /who/knows/where -l X11

	Brian Hammond 2/9/96.    Feel free to do with this as you will!
*/


/* include the X library headers */
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>

/* include some silly stuff */
#include <stdio.h>
#include <stdlib.h>

/* here are our X variables */
Display *dis;
int screen;
Window win;
GC gc;

/* here are our X routines declared! */
void init_x();
void close_x();
void redraw();

XEvent event;		/* the XEvent declaration !!! */
KeySym key;		/* a dealie-bob to handle KeyPress Events */
char text[255];		/* a char buffer for KeyPress Events */

/* Declarations for drawing */
struct FirstPoint {
	int set;
	int x, y;
};
struct FirstPoint first_point = {0, 0, 0};

enum Mode {Line, MultiLine, Rect, Free};
enum Mode current_mode = Line;

int x, y;

int min(int a, int b)
{
	return a > b ? b : a;
}

int max (int a, int b)
{
	return a > b ? a : b;
}

void check_key_press()
{
	switch (text[0]) {
	case 'q':
		close_x(); break;
	case 'l':
		current_mode = Line;
		first_point.set = 0;
		printf("Line mode set\n");
		break;
	case 'm':
		current_mode = MultiLine;
		first_point.set = 0;
		printf("MultiLine mode set\n");
		break;
	case 'r':
		current_mode = Rect;
		first_point.set = 0;
		printf("Rect mode set\n");
		break;
	case 'f':
		current_mode = Free;
		first_point.set = 0;
		printf("Free mode set\n");
		break;
	}
}

void line_mode()
{
	if (!first_point.set) {
		first_point.set = 1;
		first_point.x = x;
		first_point.y = y;
		printf("First point set at %d, %d\n", x, y);
	} else {
		XDrawLine(dis, win, gc, x, y, first_point.x, first_point.y);
		first_point.set = 0;
		printf("Second point set at %d, %d\n", x, y);
	}
}

void multiline_mode()
{
	if (!first_point.set) {
		first_point.set = 1;
		first_point.x = x;
		first_point.y = y;
		printf("First point set at %d, %d\n", x, y);
	} else {
		XDrawLine(dis, win, gc, x, y, first_point.x, first_point.y);
		first_point.x = x;
		first_point.y = y;
		printf("Next point set at %d, %d\n", x, y);
	}
}

void rect_mode()
{
	if (!first_point.set) {
		first_point.set = 1;
		first_point.x = x;
		first_point.y = y;
		printf("First point set at %d, %d\n", x, y);
	} else {
		int top_left_x = min(x, first_point.x);
		int top_left_y = min(y, first_point.y);
		XDrawRectangle(dis, win, gc, top_left_x, top_left_y,
			       abs(x - first_point.x), abs(y - first_point.y));
		first_point.set = 0;
		printf("Second point set at %d, %d\n", x, y);
	}
}

void free_mode()
{
	if (!first_point.set) {
		first_point.set = 1;
	} else {
		XDrawLine(dis, win, gc, x, y, first_point.x, first_point.y);
	}

	first_point.x = x;
	first_point.y = y;
}

void check_button_press()
{
	/* tell where the mouse Button was Pressed */
	x = event.xbutton.x;
	y = event.xbutton.y;

	if (event.xbutton.button == Button1) {
		switch (current_mode) {
		case Line:
			line_mode();
			break;
		case MultiLine:
			multiline_mode();
			break;
		case Rect:
			rect_mode();
			break;
		default:
			break;
		}
	} else if (event.xbutton.button == Button2) {
		first_point.set = 0;
		redraw();
	} else if (event.xbutton.button == Button3) {
		first_point.set = 0;
	}
}

void check_motion()
{
	x = event.xmotion.x;
	y = event.xmotion.y;

	switch (current_mode) {
	case Free:
		free_mode();
		break;
	default:
		break;
	}
}

void check_button_release()
{
	switch (current_mode) {
	case Free:
		first_point.set = 0;
		break;
	default:
		break;
	}
}

void check_event()
{
	/* get the next event and stuff it into our event variable.
	   Note:  only events we set the mask for are detected!
	*/
	XNextEvent(dis, &event);

	if (event.type==Expose && event.xexpose.count == 0) {
	/* the window was exposed redraw it! */
		redraw();
	}
	if (event.type==KeyPress &&
	    XLookupString(&event.xkey, text, 255, &key, 0) == 1) {
	/* use the XLookupString routine to convert the invent
	   KeyPress data into regular text.  Weird but necessary...
	*/
		check_key_press();
	}
	if (event.type == ButtonPress) {
		check_button_press();
	}
	if (event.type == MotionNotify) {
		check_motion();
	}
	if (event.type == ButtonRelease) {
		check_button_release();
	}
}

int main() {
	init_x();

	/* look for events forever... */
	while(1) {
		check_event();
	}
}

void init_x() {
/* get the colors black and white (see section for details) */
	unsigned long black,white;

	dis=XOpenDisplay((char *)0);
   	screen=DefaultScreen(dis);
	black=BlackPixel(dis,screen),
	white=WhitePixel(dis,screen);
   	win=XCreateSimpleWindow(dis, DefaultRootWindow(dis), 0, 0,
		300, 300, 0,black, white);
	XSetStandardProperties(dis,win,"Skicar","Hi",None,NULL,0,NULL);
	XSelectInput(dis, win, ExposureMask|ButtonPressMask|KeyPressMask|Button1MotionMask|ButtonReleaseMask);
        gc=XCreateGC(dis, win, 0,0);
	XSetBackground(dis,gc,white);
	XSetForeground(dis,gc,black);
	XClearWindow(dis, win);
	XMapRaised(dis, win);
};

void close_x() {
	XFreeGC(dis, gc);
	XDestroyWindow(dis,win);
	XCloseDisplay(dis);
	exit(1);
};

void redraw() {
	XClearWindow(dis, win);
};
